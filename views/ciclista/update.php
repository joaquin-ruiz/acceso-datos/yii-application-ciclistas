<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ciclista */

$this->title = Yii::t('app', 'Update Ciclista: {name}', [
    'name' => $model->dorsal,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ciclistas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dorsal, 'url' => ['view', 'id' => $model->dorsal]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ciclista-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
