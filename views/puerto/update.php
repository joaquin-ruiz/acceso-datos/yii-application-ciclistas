<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Puerto */

$this->title = Yii::t('app', 'Update Puerto: {name}', [
    'name' => $model->nompuerto,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Puertos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nompuerto, 'url' => ['view', 'id' => $model->nompuerto]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="puerto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
