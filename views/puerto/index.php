<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Puertos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="puerto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Puerto'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nompuerto',
            'altura',
            'categoria',
            'pendiente',
            'numetapa',
            //'dorsal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
