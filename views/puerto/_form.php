<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Puerto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="puerto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nompuerto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'altura')->textInput() ?>

    <?= $form->field($model, 'categoria')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pendiente')->textInput() ?>

    <?= $form->field($model, 'numetapa')->textInput() ?>

    <?= $form->field($model, 'dorsal')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
