<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Maillot */

$this->title = Yii::t('app', 'Update Maillot: {name}', [
    'name' => $model->código,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Maillots'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->código, 'url' => ['view', 'id' => $model->código]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="maillot-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
