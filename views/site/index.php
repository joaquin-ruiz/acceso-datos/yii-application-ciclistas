<?php

/* @var $this yii\web\View 
 *
 *         _                         __        _____       _       __  __            __      
 *        | |                       /_/       |  __ \     (_)     |  \/  |          /_/      
 *        | | ___   __ _  __ _ _   _ _ _ __   | |__) |   _ _ ____ | \  / | ___  _ __ _ _ __  
 *    _   | |/ _ \ / _` |/ _` | | | | | '_ \  |  _  / | | | |_  / | |\/| |/ _ \| '__| | '_ \ 
 *   | |__| | (_) | (_| | (_| | |_| | | | | | | | \ \ |_| | |/ /  | |  | | (_) | |  | | | | |
 *    \____/ \___/ \__,_|\__, |\__,_|_|_| |_| |_|  \_\__,_|_/___| |_|  |_|\___/|_|  |_|_| |_|
 *                          | |                                                              
 *                          |_|                                                              
 */

$this->title = 'Joaquin App';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>¡Felicidades!</h1>

        <p class="lead">Bienvenido a tu primera página con Yii 2.0</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">
      
       <?php echo \Yii::$app->view->renderFile('@app/common/consultas.php'); ?>

    </div>
</div>
