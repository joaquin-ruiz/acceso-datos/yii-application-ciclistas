<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Etapa */

$this->title = Yii::t('app', 'Update Etapa: {name}', [
    'name' => $model->numetapa,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Etapas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->numetapa, 'url' => ['view', 'id' => $model->numetapa]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="etapa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
