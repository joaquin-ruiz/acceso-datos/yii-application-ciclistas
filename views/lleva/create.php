<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lleva */

$this->title = Yii::t('app', 'Create Lleva');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Llevas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lleva-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
