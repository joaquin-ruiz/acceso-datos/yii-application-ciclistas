<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Lleva */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lleva-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dorsal')->textInput() ?>

    <?= $form->field($model, 'numetapa')->textInput() ?>

    <?= $form->field($model, 'código')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
