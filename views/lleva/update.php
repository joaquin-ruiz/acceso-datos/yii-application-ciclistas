<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lleva */

$this->title = Yii::t('app', 'Update Lleva: {name}', [
    'name' => $model->numetapa,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Llevas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->numetapa, 'url' => ['view', 'numetapa' => $model->numetapa, 'código' => $model->código]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="lleva-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
