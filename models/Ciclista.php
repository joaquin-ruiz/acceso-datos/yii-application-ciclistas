<?php

namespace app\models;

use Yii;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "ciclista".
 *
 * @property int $dorsal
 * @property string $nombre
 * @property int|null $edad
 * @property string $nomequipo
 *
 * @property Equipo $nomequipo0
 * @property Etapa[] $etapas
 * @property Lleva[] $llevas
 * @property Puerto[] $puertos
 */
class Ciclista extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ciclista';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dorsal', 'nombre', 'nomequipo'], 'required'],
            [['dorsal', 'edad'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['nomequipo'], 'string', 'max' => 25],
            [['dorsal'], 'unique'],
            [['nomequipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipo::className(), 'targetAttribute' => ['nomequipo' => 'nomequipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dorsal' => Yii::t('app', 'Dorsal'),
            'nombre' => Yii::t('app', 'Nombre'),
            'edad' => Yii::t('app', 'Edad'),
            'nomequipo' => Yii::t('app', 'Nomequipo'),
        ];
    }

    /**
     * Gets query for [[Nomequipo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNomequipo0()
    {
        return $this->hasOne(Equipo::className(), ['nomequipo' => 'nomequipo']);
    }

    /**
     * Gets query for [[Etapas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEtapas()
    {
        return $this->hasMany(Etapa::className(), ['dorsal' => 'dorsal']);
    }

    /**
     * Gets query for [[Llevas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLlevas()
    {
        return $this->hasMany(Lleva::className(), ['dorsal' => 'dorsal']);
    }

    /**
     * Gets query for [[Puertos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPuertos()
    {
        return $this->hasMany(Puerto::className(), ['dorsal' => 'dorsal']);
    }
}
