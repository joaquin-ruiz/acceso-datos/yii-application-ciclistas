<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipo".
 *
 * @property string $nomequipo
 * @property string|null $director
 *
 * @property Ciclista[] $ciclistas
 */
class Equipo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nomequipo'], 'required'],
            [['nomequipo'], 'string', 'max' => 25],
            [['director'], 'string', 'max' => 30],
            [['nomequipo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nomequipo' => Yii::t('app', 'Nomequipo'),
            'director' => Yii::t('app', 'Director'),
        ];
    }

    /**
     * Gets query for [[Ciclistas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCiclistas()
    {
        return $this->hasMany(Ciclista::className(), ['nomequipo' => 'nomequipo']);
    }
}
