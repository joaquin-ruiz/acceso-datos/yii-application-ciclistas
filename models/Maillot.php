<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maillot".
 *
 * @property string $código
 * @property string $tipo
 * @property string $color
 * @property int $premio
 *
 * @property Lleva[] $llevas
 * @property Etapa[] $numetapas
 */
class Maillot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maillot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['código', 'tipo', 'color', 'premio'], 'required'],
            [['premio'], 'integer'],
            [['código'], 'string', 'max' => 3],
            [['tipo'], 'string', 'max' => 30],
            [['color'], 'string', 'max' => 20],
            [['código'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'código' => Yii::t('app', 'Código'),
            'tipo' => Yii::t('app', 'Tipo'),
            'color' => Yii::t('app', 'Color'),
            'premio' => Yii::t('app', 'Premio'),
        ];
    }

    /**
     * Gets query for [[Llevas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLlevas()
    {
        return $this->hasMany(Lleva::className(), ['código' => 'código']);
    }

    /**
     * Gets query for [[Numetapas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNumetapas()
    {
        return $this->hasMany(Etapa::className(), ['numetapa' => 'numetapa'])->viaTable('lleva', ['código' => 'código']);
    }
}
