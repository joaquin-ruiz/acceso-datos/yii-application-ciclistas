<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "puerto".
 *
 * @property string $nompuerto
 * @property int $altura
 * @property string $categoria
 * @property float|null $pendiente
 * @property int $numetapa
 * @property int|null $dorsal
 *
 * @property Ciclista $dorsal0
 * @property Etapa $numetapa0
 */
class Puerto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'puerto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nompuerto', 'altura', 'categoria', 'numetapa'], 'required'],
            [['altura', 'numetapa', 'dorsal'], 'integer'],
            [['pendiente'], 'number'],
            [['nompuerto'], 'string', 'max' => 35],
            [['categoria'], 'string', 'max' => 1],
            [['nompuerto'], 'unique'],
            [['dorsal'], 'exist', 'skipOnError' => true, 'targetClass' => Ciclista::className(), 'targetAttribute' => ['dorsal' => 'dorsal']],
            [['numetapa'], 'exist', 'skipOnError' => true, 'targetClass' => Etapa::className(), 'targetAttribute' => ['numetapa' => 'numetapa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nompuerto' => Yii::t('app', 'Nompuerto'),
            'altura' => Yii::t('app', 'Altura'),
            'categoria' => Yii::t('app', 'Categoria'),
            'pendiente' => Yii::t('app', 'Pendiente'),
            'numetapa' => Yii::t('app', 'Numetapa'),
            'dorsal' => Yii::t('app', 'Dorsal'),
        ];
    }

    /**
     * Gets query for [[Dorsal0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDorsal0()
    {
        return $this->hasOne(Ciclista::className(), ['dorsal' => 'dorsal']);
    }

    /**
     * Gets query for [[Numetapa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNumetapa0()
    {
        return $this->hasOne(Etapa::className(), ['numetapa' => 'numetapa']);
    }
}
