<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lleva".
 *
 * @property int $dorsal
 * @property int $numetapa
 * @property string $código
 *
 * @property Etapa $numetapa0
 * @property Maillot $código0
 * @property Ciclista $dorsal0
 */
class Lleva extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lleva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dorsal', 'numetapa', 'código'], 'required'],
            [['dorsal', 'numetapa'], 'integer'],
            [['código'], 'string', 'max' => 3],
            [['numetapa', 'código'], 'unique', 'targetAttribute' => ['numetapa', 'código']],
            [['numetapa'], 'exist', 'skipOnError' => true, 'targetClass' => Etapa::className(), 'targetAttribute' => ['numetapa' => 'numetapa']],
            [['código'], 'exist', 'skipOnError' => true, 'targetClass' => Maillot::className(), 'targetAttribute' => ['código' => 'código']],
            [['dorsal'], 'exist', 'skipOnError' => true, 'targetClass' => Ciclista::className(), 'targetAttribute' => ['dorsal' => 'dorsal']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dorsal' => Yii::t('app', 'Dorsal'),
            'numetapa' => Yii::t('app', 'Numetapa'),
            'código' => Yii::t('app', 'Código'),
        ];
    }

    /**
     * Gets query for [[Numetapa0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNumetapa0()
    {
        return $this->hasOne(Etapa::className(), ['numetapa' => 'numetapa']);
    }

    /**
     * Gets query for [[Código0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCódigo0()
    {
        return $this->hasOne(Maillot::className(), ['código' => 'código']);
    }

    /**
     * Gets query for [[Dorsal0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDorsal0()
    {
        return $this->hasOne(Ciclista::className(), ['dorsal' => 'dorsal']);
    }
}
