<?php 

use yii\helpers\Html;

$ar_button = [    
    'class' => ['btn', 'btn-default'],
];
$dao_button = [
    'class' => ['btn', 'btn-primary']
];
$enunciado = array(
                    "Listar las edades de los ciclistas (sin repetidos)",
                    "Listar las edades de los ciclistas de Artiach",
                    "Listar las edades de los ciclistas de Artiach o de Amore Vita",
                    "Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
                    "Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
                    "Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
                    "Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
                    "Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
                    "Listar el nombre de los puertos cuya altura sea mayor de 1500",
                    "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
                    "Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000"
    );
?> 

<div class="row">

<?php
for ($i = 0; $i < sizeof($enunciado); $i++) {
   ?>

    <div class="col-lg-4">
        <?=
            // tag($name, $content, $options[])
            Html::tag('h2','Consulta ' . ($i + 1));
            echo   Html::tag('p', $enunciado[$i]);
            //  a($text, $url, $options[])
            echo   Html::a('Boton AR', 'site/result?idb=' . $i . '&method=ar', $ar_button);
            echo   Html::a('Boton DAO', 'site/result?idb=' . $i . '&method=dao', $dao_button);
        ?>      
    </div>

 <?php       
} 
 //return ob_get_clean();
?>

    </div>
