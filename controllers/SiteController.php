<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Ciclista;
use app\models\Puerto;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */ 
    
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionResult($idb, $method)
    {   
      
        // get the posts in the current page
        //  $posts = $provider->getModels();
        
       $enunciado = SiteController::getPetition();
       $campo = SiteController::getFields();
       $sql = SiteController::getSQLString();
               
    
        if ($method == 'ar') {
            $query = SiteController::getARQuery($idb);
             $provider = new ActiveDataProvider([
                'query' => $query,                   
                'pagination' => [
                    'pageSize' => 10,
                    ],
                ]);
             $method = 'Active Record';
        } else {        
            $query = SiteController::getDAOQuery($idb);  
            $provider = new SqlDataProvider([
                'sql' => $query,
                'totalCount' => Yii::$app->db->createCommand($query)->queryScalar(),
                'pagination' => [
                    'pageSize' => 10,
                    ],
                ]);
            $method = 'Data Access Objects';
        }        
           
           
        
        if ($idb == 6) {
            return $this->render("result",[
                "resultado" =>  $provider,
                "campos"    => ['nombre','dorsal'],
                "title"     => "<h1 class='text-center'>Consulta Nº" . ($idb + 1) .
                               "</h1><h3 class='text-center'>" . $method . "</h3>",
                "enunciado" =>  $enunciado[$idb],
                "sql"       =>  $sql[$idb],
            ]);
        }    
            
        return $this->render("result",[
            "resultado" =>  $provider,
            "campos"    =>  [$campo[$idb]],
            "title"     =>  "<h1 class='text-center'>Consulta Nº" . ($idb + 1) .
                            "</h1><h3 class='text-center'>" . $method . "</h3>",
            "enunciado" =>  $enunciado[$idb],
            "sql"       =>  $sql[$idb],
        ]);
               
    }
    
    public function getPetition() {
        
        $enunciado = array(
                    "Listar las edades de los ciclistas (sin repetidos)",
                    "Listar las edades de los ciclistas de Artiach",
                    "Listar las edades de los ciclistas de Artiach o de Amore "
                    . "Vita",
                    "Listar los dorsales de los ciclistas cuya edad sea menor "
                    . "que 25 o mayor que 30",
                    "Listar los dorsales de los ciclistas cuya edad esté entre "
                    . "28 y 32 y además que sólo sean de Banesto",
                    "Indícame el nombre de los ciclistas que el número de "
                    . "caracteres del nombre sea mayor que 8",
                    "Lístame el nombre y el dorsal de todos los ciclistas "
                    . "mostrando un campo nuevo denominado nombre mayúsculas "
                    . "que debe mostrar el nombre en mayúsculas",
                    "Listar todos los ciclistas que han ganado el maillot MGE "
                    . "(amarillo) en alguna etapa",
                    "Listar el nombre de los puertos cuya altura sea mayor de "
                    . "1500",
                    "Listar el dorsal de los ciclistas que hayan ganado algun "
                    . "puerto cuya pendiente sea mayor que 8 o cuya altura esté"
                    . "entre 1800 y 3000",
                    "Listar el dorsal de los ciclistas que hayan ganado algún "
                    . "puerto cuya pendiente sea mayor que 8 y cuya altura esté"
                    . "entre 1800 y 3000"
        );
        
        return $enunciado;
        
    }
    
     public function getFields() {
        
        $campos = array(
                    'edad',
                    'edad',
                    'edad',
                    'dorsal',
                    'dorsal',
                    'nombre',
                    null,
                    'nombre',
                  'nompuerto',
                    'dorsal',
                    'dorsal'
        );
        
        return $campos;
        
    }
    
    public function getARQuery($idb){
        switch ($idb) {
                case 0:
                    $query = Ciclista::find()->select('edad')->distinct();
                    break;
                case 1:
                    $query = Ciclista::find()->select('edad')
                             ->where('nomequipo' == 'Artiach');
                    break;
                case 2:
                    // Error
                    $query = Ciclista::find()->select('edad')->where('nomequipo'
                            == 'Artiach')->orWhere('nomequipo' == 'Amore Vita');
                    break;
                case 3:
                    $query = Ciclista::find()->select('dorsal')
                        ->where('edad < 25')->orWhere('edad > 30');   
                    break;
                case 4:
                   $query = Ciclista::find()->select('dorsal')->where('edad
                    between 28 and 35', 'nomequipo' == 'Banesto');
                    break;
                case 5:
                     $query = Ciclista::findBySql('SELECT `nombre` FROM `ciclista`'
                                        . ' WHERE CHAR_LENGTH(`nombre`) > 8');
                    break;
                case 6:
                    // El Alias no funciona. Sin forma de obtener la consulta en mayusculas
                    $query = Ciclista::find()->select(['nombre AS `nombre mayúsculas`', 'dorsal']);
                    break;
                case 7:
                    // No consigo filtrar los maillot.color = amarillo
                    $query = Ciclista::find()->select('nombre')
                             ->join('join', 'lleva', 'ciclista.dorsal = lleva.dorsal')
                             ->join('join', 'maillot', 'lleva.código = maillot.código')
                             ->groupBy('nombre');
                    break;
                case 8:
                    $query = Puerto::find()->select('nompuerto')->where('altura' > 1500);
                    break;
                case 9:
                    $query = Ciclista::find()->select('ciclista.dorsal')
                        ->join('join', 'puerto', 'ciclista.dorsal = puerto.dorsal')
                        ->where('puerto.pendiente > 8')
                        ->orWhere('puerto.altura between 1800 and 3000');
                    break;
                case 10:
                    $query = Ciclista::find()->select('ciclista.dorsal')
                        ->join('join', 'puerto', 'ciclista.dorsal = puerto.dorsal')
                        ->where('puerto.pendiente > 8')
                        ->andWhere('puerto.altura between 1800 and 3000');
                    break;                     
        }
    return $query;
    }
    
    public function getDAOQuery($idb){

        return SiteController::getSQLString()[$idb];
        //->queryScalar();  
    }
    
    public function getSQLString(){
        
        $sql = array(
                    "SELECT DISTINCT `edad` FROM `ciclista`",
                    "SELECT `edad` FROM `ciclista` WHERE nomequipo = 'Artiach'",
                    "SELECT `edad` FROM `ciclista` WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
                    "SELECT `dorsal` FROM `ciclista` WHERE (edad < 25) OR (edad > 30)",
                    "SELECT `dorsal` FROM `ciclista` "
                                            . "WHERE edad between 28 and 32 "
                                            . "AND `nomequipo` = 'Banesto'",
                    "SELECT `nombre` FROM ciclista WHERE CHAR_LENGTH(`nombre`) > 8",
                    "SELECT nombre AS `nombre mayúsculas`, `dorsal` FROM `ciclista`",
                    "SELECT `nombre` FROM `ciclista` "
                        . "JOIN `lleva` JOIN  `maillot`"
                        . " ON (lleva.dorsal = ciclista.dorsal "
                                . "AND maillot.codigo = maillot.codigo)"
                        . "WHERE maillot.color = `Amarillo`",
                    "SELECT `nombre` FROM `ciclista` "
                    . "join `lleva` ON ciclista.dorsal = lleva.dorsal join `maillot` "
                    . "ON lleva.código = maillot.código GROUP BY `nombre`",
                    "SELECT `nompuerto` FROM `puerto`",
                    "SELECT `ciclista`.`dorsal` FROM `ciclista` "
                    . "join `puerto` ON ciclista.dorsal = puerto.dorsal "
                    . "WHERE (puerto.pendiente > 8) "
                    . "OR (puerto.altura between 1800 and 3000)",
                    "SELECT `ciclista`.`dorsal` FROM `ciclista` "
                    . "join `puerto` ON ciclista.dorsal = puerto.dorsal "
                    . "WHERE (puerto.pendiente > 8) "
                    . "AND (puerto.altura between 1800 and 3000)",
        );
        return $sql;
    }
 }
